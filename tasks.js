'use strict';
/*
    В этом задании нельзя менять разметку, только писать код в этом файле.
 */

/**
*   1. Удали со страницы элемент с id "deleteMe"
**/

function removeBlock() {
    const elem = document.getElementById('deleteMe');
    elem.remove();
}

/**
 *  2. Сделай так, чтобы во всех элементах с классом wrapper остался только один параграф,
 *  в котором будет сумма чисел из всех параграфов.
 *
 *  Например, такой элемент:
 *
 *  <div class="wrapper"><p>5</p><p>15</p><p>25</p><p>35</p></div>
 *
 *  должен стать таким
 *
 *  <div class="wrapper"><p>80</p></div>
 */

function calcParagraphs() {
  const wrappers = document.querySelectorAll('.wrapper');
  
  for (let wrapper of wrappers){
      let sum = 0;
      const paragraphs = wrapper.querySelectorAll('p');
      for (let p of paragraphs){
          sum+=Number(p.innerHTML);
          p.remove();
      }
      const pSum = document.createElement('p');
      pSum.innerHTML = sum.toString();
      wrapper.appendChild(pSum);


  }

}

/**
 *  3. Измени value и type у input c id "changeMe"
 *
 */

function changeInput() {
    const elem = document.getElementById('changeMe');
    elem.setAttribute('type','text');
    elem.setAttribute('value','1234');
}

/**
 *  4. Используя функции appendChild и insertBefore дополни список с id "changeChild"
 *  чтобы получилась последовательность <li>0</li><li>1</li><li>2</li><li>3</li>
 *
 */

function appendList() {
    const changeChild = document.getElementById('changeChild');
    const listOfLi = changeChild.querySelectorAll('li');
    const beforeLi = document.createElement('li');
    beforeLi.innerHTML = '1';
    const afterLi = document.createElement('li');
    afterLi.innerHTML = '3';
    changeChild.insertBefore(beforeLi,listOfLi[1]);
    changeChild.appendChild(afterLi);

}

/**
 *  5. Поменяй цвет всем div с class "item".
 *  Если у блока class "red", то замени его на "blue" и наоборот для блоков с class "blue"
 */

function changeColors() {
    const blocks = document.querySelectorAll('.item');
    blocks.forEach((block)=>{
        block.classList.toggle('blue');
        block.classList.toggle('red');
    })
}

removeBlock();
calcParagraphs();
changeInput();
appendList();
changeColors();
