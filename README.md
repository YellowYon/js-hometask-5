# js-hometask-5

## Ответьте на вопросы

1. Какие данные хранятся в следующих свойствах узла: `parentElement`, `children`, `previousElementSibling` и `nextElementSibling`?
>parentElement - ссылка на Element_Node родителя.\
children - HTMLCollection из всех вложенных дочерних Element_Node\
previousElementSibling - предыдущий узел Element_Node\
nextElementSibling - см previousElementSibling, только следующий
2. Нарисуй какое получится дерево для следующего HTML-блока.
![diagram](./img/diag.png)

```html
<p>Hello,<!--MyComment-->World!</p>
```

## Выполните задания

1. Клонируй репозиторий;
2. Допиши функции в `tasks.js`;
3. Для проверки работы функций открой index.html в браузере;
4. Создай MR с решением.
